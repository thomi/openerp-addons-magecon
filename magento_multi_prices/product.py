from openerp.osv import osv
from openerp.osv import fields
import openerp.addons.decimal_precision as dp

class product_product(osv.osv):
    _inherit = 'product.product'

    _columns = {
        'price_usd': fields.float(
            'USD Price', digits_compute=dp.get_precision('Product Price')),
        'price_eup': fields.float(
            'EUR Price', digits_compute=dp.get_precision('Product Price')),
        'shop_tax_ids': fields.one2many('shop.tax.product', 'product_id',
                                        'Tax Association'),
    }

product_product()

class shop_tax_product(osv.osv):
    _name = 'shop.tax.product'
    _description = 'Shop wise tax for every product'

    _columns = {
        'shop_id':fields.many2one('sale.shop', 'Shop', domain=[('magento_shop', '=', True)]),
        'product_id': fields.many2one('product.product', 'Product'),
        'tax_id': fields.many2one('magerp.product_attribute_options', 'Tax',
                                  domain=[('attribute_id.attribute_code', '=',
                                           'tax_class_id')])
    }

shop_tax_product()


