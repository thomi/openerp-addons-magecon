# -*- coding: utf-8 -*-
from openerp.osv import osv
from openerp.osv import fields
from base_external_referentials.external_osv import ExternalSession

import logging

_logger = logging.getLogger(__name__)

class product_link(osv.osv):
    _inherit = 'product.link'

    def _get_link_type_selection(self, cr, uid, context=None):
        return [('cross_sell', 'Cross-Sell'), ('up_sell', 'Up-Sell'),
                ('related', 'Related'), ('associated', 'Associated')]
    _columns = {
        'type': fields.selection(_get_link_type_selection, 'Link type',
                                 required=True),
    }

product_link()


class product_product(osv.osv):
    _inherit = 'product.product'

    _columns = {
        'config_attribute': fields.many2one('magerp.product_attributes',
                                            'Configurable Atttribute'),
        'magento_exportable': fields.boolean('Magento Sync'),

    }
    _defaults = {
        'magento_exportable': lambda *a: False,
    }

product_product()


class external_referencial(osv.osv):
    _inherit = 'external.referential'

    def get_config_data(self, cr, uid, p_id, external_session):
        print "ttttttttttttttttttttttttttttttttttttttttttttttttttttttTT"
        conn = external_session.connection
        ext_id = self.pool.get('product.product').get_extid(
            cr, uid, p_id, external_session.referential_id.id)
        product_links = conn.call('product_link.list', ['associated',
                                                        ext_id])

        return product_links

    def sync_products(self, cr, uid, ids, context=None):
        res = super(external_referencial, self).sync_products(cr, uid, ids,
                                                              context=context)
        
        _logger.info("start synchronizing associated links")
        
        cr.commit()

        product_pool = self.pool.get('product.product')
        pro_links_pool = self.pool.get('product.link')
        p_ids = product_pool.search(cr, uid, [('product_type',
                                               '=',
                                               'configurable')])
        for referential in self.browse(cr, uid, ids, context):
            external_session = ExternalSession(referential, referential)
            for p_id in p_ids:
                _logger.info("product id: %s", p_id)
                try:
                    config_data = self.get_config_data(cr, uid, p_id,
                                                       external_session)
                    _logger.info("config_data: %s", config_data)
                    if config_data:
                        sr_id = pro_links_pool.search(cr, uid,
                                                      [('product_id', '=',
                                                        p_id)])
                        pro_links_pool.unlink(cr, uid, sr_id)
                    for att_id in config_data:
                        att_oeid = self.pool.get(
                            'magerp.product_attributes'
                        ).get_oeid(
                            cr, uid, att_id,
                            external_session.referential_id.id)
                        product_pool.write(cr, uid, p_id,
                                           {'config_attribute': att_oeid})
                        for link_p_id in config_data[att_id]:
                            link_oeid = product_pool.get_oeid(
                                cr, uid,
                                link_p_id,
                                external_session.referential_id.id)
                            
                            link_dict = {'type': 'associated',
                                          'is_active': True,
                                          'product_id': p_id,
                                          'linked_product_id': link_oeid}
                            _logger.info("link dict: %s", link_dict)
                            pl_id = pro_links_pool.create(
                                cr, uid, {'type': 'associated',
                                          'is_active': True,
                                          'product_id': p_id,
                                          'linked_product_id': link_oeid})
                            _logger.info("product link id: %s", pl_id)
                            
                except Exception as e:
                    _logger.error("failed to synch associated links, error : %s", e, exc_info=True)
                    continue

        return res

external_referencial()
