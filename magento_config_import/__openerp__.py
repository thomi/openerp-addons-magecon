{
    "name" : "Magento configurable product import",
    "version" : "6.1.2",
    "depends" : ["magentoerpconnect",
                ],
    "author" : "",
    "description": """Magento E-commerce management
""",
    'images': [
    ],
    "website" : "",
    "category" : "Generic Modules",
    "init_xml" : [],
    "demo_xml" : [],
    "update_xml" : [
        "product_view.xml",
                    ],
    "auto_install": False,
    "installable": True,
    'application': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

